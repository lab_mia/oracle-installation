#!/bin/env zsh
dnf update -y
dnf install -y bc \
 binutils \
 elfutils-libelf \
 elfutils-libelf-devel \
 fontconfig-devel \
 glibc \
 glibc-devel \
 ksh \
 libaio \
 libaio-devel \
 libXrender \
 libXrender-devel \
 libX11 \
 libXau \
 libXi \
 libXtst \
 libgcc \
 librdmacm-devel \
 libstdc++ \
 libstdc++-devel \
 libxcb \
 make \
 net-tools \
 smartmontools \
 sysstat \
 unzip \
 libnsl \
 libnsl2

echo Creando grupos y usuarios para configurar el entorno

groupadd -g 1501 oinstall
groupadd -g 1502 dba
groupadd -g 1503 oper
groupadd -g 1504 backupdba
groupadd -g 1505 dgdba
groupadd -g 1506 kmdba
groupadd -g 1507 racdba
useradd -u 1501 -g oinstall -G dba,oper,backupdba,dgdba,kmdba,racdba oracle
echo "oracle" | passwd oracle --stdin

echo Creando las carpetas del entorno de Oracle
mkdir -p /u01/app/oracle/product/19.3.0/dbhome_1
mkdir -p /u02/oradata
chown -R oracle:oinstall /u01 /u02
chmod -R 775 /u01 /u02
