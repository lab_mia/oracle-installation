# Instalación física de Oracle 19c en Linux

### Deshabilita Transparent HugePages
Las Transparent HugePages están habilitadas por default en CentOS, Redhat y Fedora.
Verifica con el comando:
```bash
# cat /sys/kernel/mm/transparent_hugepage/enabled
[always] madvise never
```
actualiza la configuración del archivo /etc/default/grub
Agrega transparent_hugepage=never
al final del campo
"GRUB_CMDLINE_LINUX". Ejemplo:

```toml
GRUB_CMDLINE_LINUX="resume=/dev/mapper/cl-swap rd.lvm.lv=cl/root rd.lvm.lv=cl/swap rhgb quiet transparent_hugepage=never"
```

Guarda las configuraciones:
```bash
grub2-mkconfig -o /boot/grub2/grub.cfg
```
reboot para replicar los cambios

Instala los prerrequisitos con el script setup.zsh

Edita el archivo de limits del sistema para proteger tu maquina y que Oracle no robe demasiados recursos

El archivo es:

```/etc/security/limits.d/30-oracle.conf```

Agrega las directivas:

```toml
oracle   soft   nofile    1024
oracle   hard   nofile    65536
oracle   soft   nproc    16384
oracle   hard   nproc    16384
oracle   soft   stack    10240
oracle   hard   stack    32768
oracle   hard   memlock    134217728
oracle   soft   memlock    134217728
```

Configura los parametros del Kernel:

```vim /etc/sysctl.d/98-oracle.conf```

```toml
fs.file-max = 6815744
kernel.sem = 250 32000 100 128
kernel.shmmni = 4096
kernel.shmall = 1073741824
kernel.shmmax = 4398046511104
kernel.panic_on_oops = 1
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576
net.ipv4.conf.all.rp_filter = 2
net.ipv4.conf.default.rp_filter = 2
fs.aio-max-nr = 1048576
net.ipv4.ip_local_port_range = 9000 65500
```

Aplica las configuraciones:
```sysctl -p```

Configura SELIUNX:

```bash
sed -i 's/^SELINUX=.*/SELINUX=permissive/g' /etc/sysconfig/selinux
setenforce permissive
# Deshabilita firewalld:
systemctl disable firewalld
systemctl stop firewalld
```

Login as Oracle:
```bash
su - oracle
```

Configura el entorno. Copia y pega el archivo bash_profile como
.bash_profile en el home de Oracle
<br>
Importa las configuraciones al entorno actual:
```bash
source ~/.bash_profile
```

Ve al link: https://www.oracle.com/database/technologies/oracle-database-software-downloads.html#19c

Instala la base de datos con el script install.zsh
Inicia sesion como root y ejecuta el script de inicializacion de la BD
```bash
su -
/u01/app/oraInventory/orainstRoot.sh
/u01/app/oracle/product/19.3.0/dbhome_1/root.sh
```

Comienza el listener de Oracle: ```lsnrctl start```

Crea tu primer base de datos con script create-db.zsh
<br>
Inicia sesion como root para inicializar la base de datos:
```bash
su -
sed -i 's/:N$/:Y/g' /etc/oratab
exit
```

Inicia sesion dentro de Oracle:
```bash
sqlplus / as sysdba
```
Ejecuta los primeros comandos para inicializar la base de datos:
```sql
ALTER SYSTEM SET DB_CREATE_FILE_DEST='/u02/oradata' SCOPE=BOTH;
ALTER PLUGGABLE DATABASE PDB1 SAVE STATE;
exit
```
